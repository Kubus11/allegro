package pl.tkluba;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;

import static pl.tkluba.Log.log;

/**
 * Singleton used to load properties
 */
public class Props
{
    private static Properties props;

    /**
     * Constructor
     */
    private Props()
    {
	log("Loading properties file...");

	props = new Properties();
	try
	{
	    InputStream inputStream = getClass().getClassLoader().getResourceAsStream("project.properties");
	    props.load(inputStream);
	    log("Properties file loaded");
	}
	catch (FileNotFoundException e)
	{
	    log("Properties file not found!");
	}
	catch (IOException e)
	{
	    log("File loading error");
	}
    }

    /**
     * Main method for testing
     */
    public static void main(String[] args)
    {
	getInstance().printAllProperties();
    }

    /**
     * Returns Props instance
     *
     * @return Props instance
     */
    public static Props getInstance()
    {
	return SingletonHolder.INSTANCE;
    }

    /**
     * Gets property
     *
     * @param key key
     * @return property
     */
    public String getProperty(String key)
    {
	return props.getProperty(key);
    }

    /**
     * Prints all properties
     */
    public void printAllProperties()
    {
	Enumeration<Object> keys = props.keys();
	while (keys.hasMoreElements())
	{
	    String key = (String) keys.nextElement();
	    String value = (String) props.get(key);
	    System.out.println(key + ": " + value);
	}
    }

    /**
     * Singleton holder
     */
    private static class SingletonHolder
    {
	private static final Props INSTANCE = new Props();
    }
}

package pl.tkluba;

import java.io.IOException;

import static pl.tkluba.Log.log;

/**
 * Singleton used to get auth token
 */
public class TokenHolder
{
    private static String token;

    /**
     * Constructor
     */
    private TokenHolder()
    {
	log("Getting the auth token...");
	try
	{
	    token = ApiController.getAuthToken();
	}
	catch (IOException exception)
	{
	    log("Unable to get the auth token");
	    log("Exception: " + exception.getMessage());
	    exception.printStackTrace();
	}
    }

    /**
     * Main method for testing
     */
    public static void main(String[] args)
    {
	log(getInstance().getToken());
    }

    /**
     * Returns TokenHolder instance
     *
     * @return TokenHolder instance
     */
    public static TokenHolder getInstance()
    {
	return SingletonHolder.INSTANCE;
    }

    public String getToken()
    {
	return token;
    }

    /**
     * Singleton holder
     */
    private static class SingletonHolder
    {
	private static final TokenHolder INSTANCE = new TokenHolder();
    }
}

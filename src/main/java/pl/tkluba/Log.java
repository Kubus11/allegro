package pl.tkluba;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Class responsible for logging
 */
public class Log
{
    private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private final String log;

    /**
     * Constructor
     */
    @SuppressWarnings("unused")
    public Log()
    {
	log = "";
    }

    /**
     * Prints given message to a console
     *
     * @param message message to be printed
     */
    public static void log(Object message)
    {
	String messageString = LocalDateTime.now().format(TIME_FORMATTER) + " " + message;
	System.out.println(messageString);
    }

    @SuppressWarnings("unused")
    public static DateTimeFormatter getTimeFormatter()
    {
	return TIME_FORMATTER;
    }

    @SuppressWarnings("unused")
    public String getLog()
    {
	return log;
    }
}

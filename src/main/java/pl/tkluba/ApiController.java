package pl.tkluba;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import static pl.tkluba.Log.log;

/**
 * Class for Allegro API connection
 */
public class ApiController
{
    private static final String BASE_URL = "https://api.allegro.pl";

    /**
     * Gets auth token
     *
     * @return auth token
     * @throws IOException exception
     */
    public static String getAuthToken() throws IOException
    {
	Props props = Props.getInstance();
	String clientId = props.getProperty("client_id");
	String clientSecret = props.getProperty("client_secret");
	String auth = clientId + ":" + clientSecret;
	byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes());
	String authHeaderValue = "Basic " + new String(encodedAuth);
	String requestUrl = "https://allegro.pl/auth/oauth/token?grant_type=client_credentials";
	HttpURLConnection connection = getPostConnection(requestUrl);
	connection.setRequestProperty("Authorization", authHeaderValue);
	JSONObject serviceResponse = new JSONObject(getServiceResponse(connection));

	return (String) serviceResponse.get("access_token");
    }

    /**
     * Creates new POST HttpURLConnection
     *
     * @param requestUrl request URL
     * @return HttpURLConnection
     * @throws IOException exception
     */
    private static HttpURLConnection getPostConnection(String requestUrl) throws IOException
    {
	URL url = new URL(requestUrl);
	HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	connection.setRequestMethod("POST");

	return connection;
    }

    /**
     * Creates new GET HttpURLConnection
     *
     * @param requestUrl request URL
     * @return HttpURLConnection
     * @throws IOException exception
     */
    private static HttpURLConnection getConnection(String requestUrl) throws IOException
    {
	HttpURLConnection connection = getUnauthorizedConnection(requestUrl);
	String token = TokenHolder.getInstance().getToken();
	connection.setRequestProperty("Authorization", "Bearer " + token);
	connection.setRequestProperty("Accept", "application/vnd.allegro.public.v1+json");

	return connection;
    }

    /**
     * Creates new GET HttpURLConnection
     *
     * @param requestUrl request URL
     * @return HttpURLConnection
     * @throws IOException exception
     */
    public static HttpURLConnection getUnauthorizedConnection(String requestUrl) throws IOException
    {
	URL url = new URL(requestUrl);
	HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	connection.setRequestMethod("GET");

	return connection;
    }

    /**
     * Gets response code for given api call
     *
     * @param connection connection
     * @return response code
     */
    public static int getResponseCode(HttpURLConnection connection)
    {
	log("Getting response code for URL: " + connection.getURL());
	int status = 0;

	try
	{
	    status = connection.getResponseCode();
	}
	catch (IOException exception)
	{
	    log("Exception: " + exception.getMessage());
	    exception.printStackTrace();
	}
	finally
	{
	    connection.disconnect();
	}

	log("Response code is: " + status);
	return status;
    }

    /**
     * Gets response code for given api call
     *
     * @param requestUrl request URL
     * @return response code
     */
    public static int getResponseCode(String requestUrl)
    {
	HttpURLConnection connection = null;
	String url = BASE_URL + requestUrl;
	int status = 0;

	try
	{
	    connection = getConnection(url);
	    status = getResponseCode(connection);
	}
	catch (IOException exception)
	{
	    log("Exception: " + exception.getMessage());
	    exception.printStackTrace();
	}
	finally
	{
	    if (connection != null)
		connection.disconnect();
	}

	return status;
    }

    /**
     * Gets media type for given api call
     *
     * @param connection connection
     * @return response code
     */
    public static String getMediaType(HttpURLConnection connection)
    {
	log("Getting media type for URL: " + connection.getURL());
	String mediaType;
	mediaType = connection.getContentType();
	connection.disconnect();
	log("Media type is: " + mediaType);
	return mediaType;
    }

    /**
     * Gets response code for given api call
     *
     * @param requestUrl request URL
     * @return response code
     */
    public static String getMediaType(String requestUrl)
    {
	HttpURLConnection connection = null;
	String url = BASE_URL + requestUrl;
	String mediaType = "";
	try
	{
	    connection = getConnection(url);
	    mediaType = getMediaType(connection);
	}
	catch (IOException exception)
	{
	    log("Exception: " + exception.getMessage());
	    exception.printStackTrace();
	}
	finally
	{
	    if (connection != null)
		connection.disconnect();
	}

	return mediaType;
    }

    /**
     * Gets response for given api call
     *
     * @param connection connection
     * @return response
     */
    public static String getServiceResponse(HttpURLConnection connection)
    {
	log("Getting service response for URL: " + connection.getURL());
	int numberOfTries = 5;
	StringBuffer content;
	String response = "";

	for (int i = 0; i < numberOfTries; i++)
	{
	    if (i > 0)
		log("Unable to get response, trying again... (attempt number " + i + ")");

	    try
	    {
		BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8));
		String inputLine;
		content = new StringBuffer();
		while ((inputLine = in.readLine()) != null)
		    content.append(inputLine);
		in.close();
		connection.disconnect();
		response = content.toString();
	    }
	    catch (IOException exception)
	    {
		log("Exception: " + exception.getMessage());
		exception.printStackTrace();
	    }
	    finally
	    {
		connection.disconnect();
	    }

	    if (!response.isEmpty())
		break;
	}

	return response;
    }

    /**
     * Gets response for given api call
     *
     * @param requestUrl request URL
     * @return response
     */
    public static String getServiceResponse(String requestUrl)
    {
	HttpURLConnection connection = null;
	String response = "";
	String url = BASE_URL + requestUrl;
	try
	{
	    connection = getConnection(url);
	    response = getServiceResponse(connection);
	}
	catch (IOException exception)
	{
	    log("Exception: " + exception.getMessage());
	    exception.printStackTrace();
	}
	finally
	{
	    if (connection != null)
		connection.disconnect();
	}

	return response;
    }

    public static String getBaseUrl()
    {
	return BASE_URL;
    }
}

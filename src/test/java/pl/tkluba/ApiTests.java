package pl.tkluba;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

/**
 * Tests for Allegro API
 */
public class ApiTests
{
    private final static String CATEGORIES_URL = "/sale/categories";
    private final static String CATEGORIES_WITH_PARENT_URL = "/sale/categories?parent.id=709";
    private final static String CATEGORIES_WITH_PARENT_WRONG_URL = "/sale/categories?parent.id=483";
    private final static String CATEGORY_URL = "/sale/categories/709";
    private final static String CATEGORY_WRONG_URL = "/sale/categories/483";
    private final static String CATEGORY_PARAMETERS_URL = "/sale/categories/709/parameters";
    private final static String CATEGORY_PARAMETERS_WRONG_URL = "/sale/categories/483/parameters";

    /**
     * Verifies that correct categories request gets the 200 response code
     */
    @Test
    public void verifyCorrectCategoriesRequestResponseCode()
    {
	Assert.assertEquals(200, ApiController.getResponseCode(CATEGORIES_URL));
    }

    /**
     * Verifies that correct categories with given parent request gets the 200 response code
     */
    @Test
    public void verifyCorrectCategoriesWithGivenParentRequestResponseCode()
    {
	Assert.assertEquals(200, ApiController.getResponseCode(CATEGORIES_WITH_PARENT_URL));
    }

    /**
     * Verifies that incorrect categories with given parent request gets the 404 response code
     */
    @Test
    public void verifyIncorrectCategoriesWithGivenParentRequestResponseCode()
    {
	Assert.assertEquals(404, ApiController.getResponseCode(CATEGORIES_WITH_PARENT_WRONG_URL));
    }

    /**
     * Verifies that correct categories request without authorization gets the 406 response code
     */
    @Test
    public void verifyCorrectCategoriesRequestWithoutAuthorizationResponseCode() throws IOException
    {
	HttpURLConnection connection = ApiController.getUnauthorizedConnection(ApiController.getBaseUrl() + CATEGORIES_URL);
	Assert.assertEquals(406, ApiController.getResponseCode(connection));
    }

    /**
     * Verifies that correct categories with given parent request without authorization gets the 406 response code
     */
    @Test
    public void verifyCorrectCategoriesWithGivenParentRequestWithoutAuthorizationResponseCode() throws IOException
    {
	HttpURLConnection connection = ApiController.getUnauthorizedConnection(ApiController.getBaseUrl() + CATEGORIES_WITH_PARENT_URL);
	Assert.assertEquals(406, ApiController.getResponseCode(connection));
    }

    /**
     * Verifies that incorrect categories with given parent request without authorization gets the 406 response code
     */
    @Test
    public void verifyIncorrectCategoriesWithGivenParentRequestWithoutAuthorizationResponseCode() throws IOException
    {
	HttpURLConnection connection = ApiController.getUnauthorizedConnection(ApiController.getBaseUrl() + CATEGORIES_WITH_PARENT_WRONG_URL);
	Assert.assertEquals(406, ApiController.getResponseCode(connection));
    }

    /**
     * Verifies that correct categories request gets the correct media type in response
     */
    @Test
    public void verifyCorrectCategoriesRequestMediaType()
    {
	Assert.assertEquals("application/vnd.allegro.public.v1+json;charset=UTF-8", ApiController.getMediaType(CATEGORIES_URL));
    }

    /**
     * Verifies that correct categories with given parent request gets the correct media type in response
     */
    @Test
    public void verifyCorrectCategoriesWithGivenParentRequestMediaType()
    {
	Assert.assertEquals("application/vnd.allegro.public.v1+json;charset=UTF-8", ApiController.getMediaType(CATEGORIES_WITH_PARENT_URL));
    }

    /**
     * Verifies that correct category request gets the 200 response code
     */
    @Test
    public void verifyCorrectCategoryRequestResponseCode()
    {
	Assert.assertEquals(200, ApiController.getResponseCode(CATEGORY_URL));
    }

    /**
     * Verifies that incorrect category request gets the 404 response code
     */
    @Test
    public void verifyIncorrectCategoryRequestResponseCode()
    {
	Assert.assertEquals(404, ApiController.getResponseCode(CATEGORY_WRONG_URL));
    }

    /**
     * Verifies that correct category request without authorization gets the 406 response code
     */
    @Test
    public void verifyCorrectCategoryRequestWithoutAuthorizationResponseCode() throws IOException
    {
	HttpURLConnection connection = ApiController.getUnauthorizedConnection(ApiController.getBaseUrl() + CATEGORY_URL);
	Assert.assertEquals(406, ApiController.getResponseCode(connection));
    }

    /**
     * Verifies that incorrect category request without authorization gets the 406 response code
     */
    @Test
    public void verifyIncorrectCategoryRequestWithoutAuthorizationResponseCode() throws IOException
    {
	HttpURLConnection connection = ApiController.getUnauthorizedConnection(ApiController.getBaseUrl() + CATEGORY_WRONG_URL);
	Assert.assertEquals(406, ApiController.getResponseCode(connection));
    }

    /**
     * Verifies that correct category request gets the correct media type in response
     */
    @Test
    public void verifyCorrectCategoryRequestMediaType()
    {
	Assert.assertEquals("application/vnd.allegro.public.v1+json;charset=UTF-8", ApiController.getMediaType(CATEGORY_URL));
    }

    /**
     * Verifies that correct category parameters request gets the 200 response code
     */
    @Test
    public void verifyCorrectCategoryParametersRequestResponseCode()
    {
	Assert.assertEquals(200, ApiController.getResponseCode(CATEGORY_PARAMETERS_URL));
    }

    /**
     * Verifies that incorrect category parameters request gets the 404 response code
     */
    @Test
    public void verifyIncorrectCategoryParametersRequestResponseCode()
    {
	Assert.assertEquals(404, ApiController.getResponseCode(CATEGORY_PARAMETERS_WRONG_URL));
    }

    /**
     * Verifies that correct category parameters request without authorization gets the 406 response code
     */
    @Test
    public void verifyCorrectCategoryParametersRequestWithoutAuthorizationResponseCode() throws IOException
    {
	HttpURLConnection connection = ApiController.getUnauthorizedConnection(ApiController.getBaseUrl() + CATEGORY_PARAMETERS_URL);
	Assert.assertEquals(406, ApiController.getResponseCode(connection));
    }

    /**
     * Verifies that incorrect category parameters request without authorization gets the 406 response code
     */
    @Test
    public void verifyIncorrectCategoryParametersRequestWithoutAuthorizationResponseCode() throws IOException
    {
	HttpURLConnection connection = ApiController.getUnauthorizedConnection(ApiController.getBaseUrl() + CATEGORY_PARAMETERS_WRONG_URL);
	Assert.assertEquals(406, ApiController.getResponseCode(connection));
    }

    /**
     * Verifies that correct category parameters request gets the correct media type in response
     */
    @Test
    public void verifyCorrectCategoryParametersRequestMediaType()
    {
	Assert.assertEquals("application/vnd.allegro.public.v1+json;charset=UTF-8", ApiController.getMediaType(CATEGORY_PARAMETERS_URL));
    }

    /**
     * Verifies category response body
     *
     * @param category category
     * @return category name
     */
    private String verifyCategoryResponseBody(JSONObject category)
    {
	Assert.assertTrue(category.has("parent"));
	if (!category.isNull("parent"))
	{
	    JSONObject parent = (JSONObject) category.get("parent");
	    Assert.assertTrue(parent.has("id"));
	    Assert.assertTrue(parent.get("id") instanceof String);
	}
	Assert.assertTrue(category.has("name"));
	Object name = category.get("name");
	Assert.assertTrue(name instanceof String);
	Assert.assertTrue(category.has("options"));
	Assert.assertTrue(category.has("id"));
	Assert.assertTrue(category.get("id") instanceof String);
	Assert.assertTrue(category.has("leaf"));
	Assert.assertTrue(category.get("leaf") instanceof Boolean);

	JSONObject options = (JSONObject) category.get("options");
	Assert.assertNotNull(options);
	Assert.assertTrue(options.has("offersWithProductPublicationEnabled"));
	Assert.assertTrue(options.get("offersWithProductPublicationEnabled") instanceof Boolean);
	Assert.assertTrue(options.has("productEANRequired"));
	Assert.assertTrue(options.get("productEANRequired") instanceof Boolean);
	Assert.assertTrue(options.has("customParametersEnabled"));
	Assert.assertTrue(options.get("customParametersEnabled") instanceof Boolean);
	Assert.assertTrue(options.has("productCreationEnabled"));
	Assert.assertTrue(options.get("productCreationEnabled") instanceof Boolean);
	Assert.assertTrue(options.has("advertisementPriceOptional"));
	Assert.assertTrue(options.get("advertisementPriceOptional") instanceof Boolean);
	Assert.assertTrue(options.has("advertisement"));
	Assert.assertTrue(options.get("advertisement") instanceof Boolean);
	Assert.assertTrue(options.has("variantsByColorPatternAllowed"));
	Assert.assertTrue(options.get("variantsByColorPatternAllowed") instanceof Boolean);

	return name.toString();
    }

    /**
     * Verifies categories response body
     *
     * @param categories list of categories
     * @return list of categories names
     */
    private List<String> verifyCategoriesResponseBody(JSONArray categories)
    {
	List<String> categoriesList = new ArrayList<>();
	for (Object object : categories)
	{
	    String name = verifyCategoryResponseBody((JSONObject) object);
	    categoriesList.add(name);
	}

	return categoriesList;
    }

    /**
     * Verifies that categories response is correct
     */
    @Test
    public void verifyCategoriesResponseBody()
    {
	JSONObject response = new JSONObject(ApiController.getServiceResponse(CATEGORIES_URL));
	JSONArray categories = response.getJSONArray("categories");
	Assert.assertEquals(13, categories.length());
	List<String> categoriesList = verifyCategoriesResponseBody(categories);
	Assert.assertEquals(13, categoriesList.size());
	Assert.assertTrue(categoriesList.contains("Dom i Ogród"));
	Assert.assertTrue(categoriesList.contains("Dziecko"));
	Assert.assertTrue(categoriesList.contains("Elektronika"));
	Assert.assertTrue(categoriesList.contains("Firma i usługi"));
	Assert.assertTrue(categoriesList.contains("Kolekcje i sztuka"));
	Assert.assertTrue(categoriesList.contains("Kultura i rozrywka"));
	Assert.assertTrue(categoriesList.contains("Moda"));
	Assert.assertTrue(categoriesList.contains("Motoryzacja"));
	Assert.assertTrue(categoriesList.contains("Nieruchomości"));
	Assert.assertTrue(categoriesList.contains("Sport i turystyka"));
	Assert.assertTrue(categoriesList.contains("Supermarket"));
	Assert.assertTrue(categoriesList.contains("Uroda"));
	Assert.assertTrue(categoriesList.contains("Zdrowie"));
    }

    /**
     * Verifies that categories with parent response is correct
     */
    @Test
    public void verifyCategoriesWithParentResponseBody()
    {
	JSONObject response = new JSONObject(ApiController.getServiceResponse(CATEGORIES_WITH_PARENT_URL));
	JSONArray categories = response.getJSONArray("categories");
	verifyCategoriesResponseBody(categories);
    }

    /**
     * Verifies that category response is correct
     */
    @Test
    public void verifyCategoryResponseBody()
    {
	JSONObject response = new JSONObject(ApiController.getServiceResponse(CATEGORY_URL));
	verifyCategoryResponseBody(response);
    }

    /**
     * Verifies that category parameters response is correct
     */
    @Test
    public void verifyCategoryParametersResponseBody()
    {
	JSONObject response = new JSONObject(ApiController.getServiceResponse(CATEGORY_PARAMETERS_URL));
	JSONArray parameters = response.getJSONArray("parameters");
	for (Object object : parameters)
	{
	    JSONObject parameter = (JSONObject) object;
	    Assert.assertTrue(parameter.has("unit"));
	    if (!parameter.isNull("unit"))
		Assert.assertTrue(parameter.get("unit") instanceof String);
	    if (!parameter.isNull("dictionary"))
	    {
		JSONArray dictionary = (JSONArray) parameter.get("dictionary");
		for (Object dictionaryObject : dictionary)
		{
		    JSONObject dictionaryJsonObject = (JSONObject) dictionaryObject;
		    Assert.assertTrue(dictionaryJsonObject.has("id"));
		    Assert.assertTrue(dictionaryJsonObject.get("id") instanceof String);
		    Assert.assertTrue(dictionaryJsonObject.has("value"));
		    Assert.assertTrue(dictionaryJsonObject.get("value") instanceof String);
		    Assert.assertTrue(dictionaryJsonObject.has("dependsOnValueIds"));
		    Assert.assertTrue(dictionaryJsonObject.get("dependsOnValueIds") instanceof JSONArray);
		}
	    }
	    Assert.assertTrue(parameter.has("name"));
	    Assert.assertTrue(parameter.get("name") instanceof String);

	    Assert.assertTrue(parameter.has("options"));
	    JSONObject options = (JSONObject) parameter.get("options");
	    Assert.assertNotNull(options);
	    Assert.assertTrue(options.has("ambiguousValueId"));
	    Assert.assertTrue(options.has("variantsAllowed"));
	    Assert.assertTrue(options.get("variantsAllowed") instanceof Boolean);
	    Assert.assertTrue(options.has("requiredDependsOnValueIds"));
	    Assert.assertTrue(options.has("dependsOnParameterId"));
	    Assert.assertTrue(options.has("customValuesEnabled"));
	    Assert.assertTrue(options.get("customValuesEnabled") instanceof Boolean);
	    Assert.assertTrue(options.has("variantsEqual"));
	    Assert.assertTrue(options.get("variantsEqual") instanceof Boolean);
	    Assert.assertTrue(options.has("displayDependsOnValueIds"));
	    Assert.assertTrue(options.has("describesProduct"));
	    Assert.assertTrue(options.get("describesProduct") instanceof Boolean);

	    Assert.assertTrue(parameter.has("restrictions"));
	    JSONObject restrictions = (JSONObject) parameter.get("restrictions");
	    Assert.assertNotNull(restrictions);
	    if (!restrictions.isNull("multipleChoices"))
		Assert.assertTrue(restrictions.get("multipleChoices") instanceof Boolean);
	    if (!restrictions.isNull("min"))
		Assert.assertTrue(restrictions.get("min") instanceof Double);
	    if (!restrictions.isNull("max"))
		Assert.assertTrue(restrictions.get("max") instanceof Double);
	    if (!restrictions.isNull("precision"))
		Assert.assertTrue(restrictions.get("precision") instanceof Integer);
	    if (!restrictions.isNull("range"))
		Assert.assertTrue(restrictions.get("range") instanceof Boolean);
	    if (!restrictions.isNull("minLength"))
		Assert.assertTrue(restrictions.get("minLength") instanceof Integer);
	    if (!restrictions.isNull("allowedNumberOfValues"))
		Assert.assertTrue(restrictions.get("allowedNumberOfValues") instanceof Integer);
	    if (!restrictions.isNull("maxLength"))
		Assert.assertTrue(restrictions.get("maxLength") instanceof Integer);

	    Assert.assertTrue(parameter.has("requiredForProduct"));
	    Assert.assertTrue(parameter.get("requiredForProduct") instanceof Boolean);
	    Assert.assertTrue(parameter.has("id"));
	    Assert.assertTrue(parameter.get("id") instanceof String);
	    Assert.assertTrue(parameter.has("type"));
	    Assert.assertTrue(parameter.get("type") instanceof String);
	    Assert.assertTrue(parameter.has("required"));
	    Assert.assertTrue(parameter.get("required") instanceof Boolean);
	}
    }
}

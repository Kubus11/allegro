# README #

Ten projekt zawiera testy napisane dla trzech endpointów Allegro z
kategorii ["Kategorie i parametry"](https://developer.allegro.pl/documentation/#tag/Categories-and-parameters):

* [GET IDs of Allegro categories](https://developer.allegro.pl/documentation/#operation/getCategoriesUsingGET)
* [GET a category by ID](https://developer.allegro.pl/documentation/#operation/getCategoryUsingGET_1)
* [GET parameters supported by a category](https://developer.allegro.pl/documentation/#operation/getFlatParametersUsingGET_2)

*Przeczytaj w innym języku: [English](README.md)*

### Jak uruchomić? ###

Testy są napisane w Javie, więc musisz mieć ją pobraną ([Pobierz Javę](https://www.java.com/pl/download/)), a także musisz mieć ustawione zmienne środowiskowe,
jeżeli pracujesz na Windowsie ([Jak ustawić zmiene środowiskowe](https://www.java.com/pl/download/help/path.html)). Jest to także projekt Mavenowy, więc musisz
go [zainstalować](https://maven.apache.org/install.html).  
Kiedy wszystko już przygotujesz, wystarczy, że przejdziesz do folderu z projektem, uruchomisz terminal i wpiszesz komendę **mvn test**.
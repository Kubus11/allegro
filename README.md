# README #

This Maven project contains tests written for three Allegro endpoints
from ["Categories and parameters category"](https://developer.allegro.pl/documentation/#tag/Categories-and-parameters):

* [GET IDs of Allegro categories](https://developer.allegro.pl/documentation/#operation/getCategoriesUsingGET)
* [GET a category by ID](https://developer.allegro.pl/documentation/#operation/getCategoryUsingGET_1)
* [GET parameters supported by a category](https://developer.allegro.pl/documentation/#operation/getFlatParametersUsingGET_2)

*Read this in other languages: [Polski](README.pl.md)*

### How to run? ###

Tests are written in Java, so you need to have it installed ([Download Java](https://www.java.com/en/download/)), and you have to set environmental variables if
you're using Windows ([How to set environmental variables](https://www.java.com/en/download/help/path.html)). This is a Maven project, so you also have
to [install it](https://maven.apache.org/install.html).  
Once you have everything set up, you just have to get to the project folder, open the terminal and run the command **mvn test**.